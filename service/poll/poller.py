import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# # Import models from service_rest, here.
# from service_rest.models import Sales_RecordVO
# # from service_rest.models import Something

# def get_records():
#     response= requests.get("http://sales-api:8000/api/records/")
#     content = json.loads(response.content)
#     for record in content["record"]:
#         Sales_RecordVO.objects.update_or_create(
#             import_href = record["href"],
#             defaults = {
#                 "vin": record["automobile"]["vin"]
#             },
#         )


def poll():
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            # get_records()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
