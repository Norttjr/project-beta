
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technicians, Appointments, Customer
from common.json import ModelEncoder
import json
# Create your views here.

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class TechniciansEncoder(ModelEncoder):
    model = Technicians
    properties = [
        "employee_number", "name"
    ]

class AppointmentsEncoder(ModelEncoder):
    model = Appointments
    properties = [
        "id",
        "vin", 
        "customer_name",
        "date", 
        "time",
        "technician", 
        "reason",
        "completed",
        "vip",
        ]
    encoders = {
        "technician": TechniciansEncoder()
    }



@require_http_methods(["GET", "POST"])
def api_technicians_list(request):
    if request.method == "GET":
        technicians = Technicians.objects.all()
        return JsonResponse(
            technicians, 
            encoder=TechniciansEncoder,
            safe=False
        )
    else: #POST
        content = json.loads(request.body)
        technicians = Technicians.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder=TechniciansEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_appointments_list(request):
    if request.method == "GET":
        appointments = Appointments.objects.all()
        return JsonResponse(
            appointments, 
            encoder=AppointmentsEncoder,
            safe=False
        )
    else: #POST
 
        content = json.loads(request.body) 
        try:
            id = content["technician"]
            technician = Technicians.objects.get(employee_number=id)
            content["technician"] = technician
        except Technicians.DoesNotExist:
            return JsonResponse(
                {"message": "That is not a valid employee number"},
                status=400,
            )
        appointment = Appointments.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentsEncoder,
            safe=False
        )


        
@require_http_methods(["DELETE"])
def api_delete_technicians(request, pk):
    technicians = Technicians.objects.get(id=pk)
    if request.method == "DELETE":
        try:
            technicians = Technicians.objects.get(id=pk)
            technicians.delete()
            return JsonResponse(
                technicians,
                encoder=TechniciansEncoder,
                safe=False,
            )
        except Technicians.DoesNotExist:
            return JsonResponse({"message": "This Tech does not exist"})

@require_http_methods(["DELETE", "PUT"])
def api_appointment(request, pk):


    if request.method == "DELETE":
        appointment = Appointments.objects.get(id=pk)
        try:
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentsEncoder,
                safe=False,
            )
        except Appointments.DoesNotExist:
            return JsonResponse({"message": "This appointment does not exist"})
    else:
        content = json.loads(request.body)
        appointment = Appointments.objects.filter(id=pk).update(**content)
        print(appointment)
        return JsonResponse(
            appointment,
            encoder=AppointmentsEncoder,
            safe=False
        )
        
        # completed_status = appointment["completed"]