from django.urls import path
from .views import api_appointment, api_technicians_list, api_appointments_list, api_delete_technicians

urlpatterns = [
    path("technicians/", api_technicians_list, name="technicians"),
    path("technicians/<int:pk>/", api_delete_technicians, name="delete_technicians"),
    path("appointments/", api_appointments_list, name="appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),


]