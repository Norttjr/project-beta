function InventoryAutosList({inventoryAutos}) {
    return (
        <div className="container">
            <h2 className="display-5 fw-bold">Automobiles</h2>
            <a href='new_auto/'>
                <h5>New Automobile</h5>
            </a>
            <a href='new_make/'>
                <h5>New Make</h5>
            </a>
            <a href='new_model'>
                <h5>New Model</h5>
            </a>
            <div className="row">
                {inventoryAutos.map(auto => {
                    return (
                        <div key={auto.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                    <h5 className="card-subtitle mb-2 text-muted">
                                        Vin Number: {auto.vin}
                                    </h5>
                                    <h6 className="card-subtitle mb-2">
                                        Year: {auto.year}
                                    </h6>
                                    <p className="card-text">
                                        Color: {auto.color}
                                    </p>
                                    <p className="card-text">
                                        Picture Url: {auto.picture_url}
                                    </p>
                                    <p className="card-text">
                                        Model: {auto.model.name}
                                    </p>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>        
        </div>
    );
}
export default InventoryAutosList;