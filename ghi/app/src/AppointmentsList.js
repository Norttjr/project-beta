import React from 'react'



class AppointmentsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            serviceAppointments: [],
            completed: true,

        };
        this.handleChangeCompleted = this.handleChangeCompleted.bind(this);
        this.handleChangeDeleted = this.handleChangeDeleted.bind(this);
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
          const data = await response.json()
          this.setState({ serviceAppointments: data.serviceAppointments })
        }
      }  

    async handleChangeCompleted(event){
        const value = event.target.value;
        this.setState({ completed: true})
        const data = {"completed": true}

        const updateUrl = `http://localhost:8080/api/appointments/${value}/`

        const fetchConfig = { 
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const response = await fetch(updateUrl, fetchConfig);
        window.location.reload();
      }

    async handleChangeDeleted(event){
        const value = event.target.value;

        const updateUrl = `http://localhost:8080/api/appointments/${value}/`

        const fetchConfig = { 
            method: "DELETE",
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const response = await fetch(updateUrl, fetchConfig);
        window.location.reload(false);
      }


    // async function cancelApp(appointment) {
    //     const deleteUrl = `http://localhost:8080/api/appointments/`
    //     const fetchConfig = {
    //         method: "DELETE"
    //     }
    //     const response = await fetch(deleteUrl, fetchConfig)
    //     window.location.reload()
    // }
    
    // async function finishApp(appointment) {
    //     const fetchConfig = {
    //         method: "PUT"
    //     }
    //     const response = await fetch(updateUrl, fetchConfig)
    //     window.location.reload()
    // }



    render(){
        return (
            <div className="container">
                <h2 className="display-5 fw-bold">Appointments</h2>
                <a href='new/'>
                    <h5>Make Appointment</h5>
                </a>
                <div className="row">
                    {this.props.serviceAppointments.filter(appInt => {
                        if (appInt.completed===false){
                            return appInt
                        }
                    }).map(appointment => {
                        return (
                            <div key={appointment.vin} className="col">
                                <div className="card mb-3 shadow">
                                    <div className="card-body">
                                        <a href={'/appointment/' + appointment.vin}>
                                            <h5 className="card-subtitle mb-2 text-muted">
                                            Name: {appointment.customer_name}
                                            </h5>
                                            <h5 className="card-subtitle mb-2 text-muted">
                                            {appointment.date}
                                            </h5>

                                        </a>
                                        <h6 className="card-subtitle mb-2 text-muted"> Appointments: {appointment.vin}</h6>
                                            <button className="btn btn-danger" onClick={this.handleChangeDeleted} value={appointment.id}>Cancel</button>
                                            <button type="button" className="btn btn-success" onClick={this.handleChangeCompleted} value={appointment.id} >Finished</button>

                                    </div>
                                </div>
                            </div>
                        );
                    })}    
                </div>        
            </div>
        );
                } 
}
export default AppointmentsList