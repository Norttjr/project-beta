

function TechniciansList(props) {
    return (
        <div className="container">
            <h2 className="display-5 fw-bold">Technicians</h2>
            <a href='new/'>
                <h5>Add Technician</h5>
            </a>
            <div className="row">
                {props.serviceTechnicians.map(technician => {
                    return (
                        <div key={technician.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                    <h5 className="card-subtitle mb-2 text-muted">
                                    {technician.name}
                                    </h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{technician.employee_number}</h6>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>        
        </div>
    );
}
export default TechniciansList