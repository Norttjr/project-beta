import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './index.css';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesEmployeeForm from './SalesEmployeeForm';
import SalesEmployeeList from './SalesEmployeeList';
import SalesEmployeeHistory from './SalesEmployeeHistory';
import SalesRecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordList';
import SaleCustomerList from './SalesCustomerList'
import SalesCustomerForm from './SalesCustomerForm';
import TechniciansList from './TechniciansList';
import TechniciansForm from './TechniciansForm';
import AppointmentsList from './AppointmentsList';
import AppointmentsForm from './AppointmentsForm';
import InventoryMakeForm from './InventoryMakeForm';
import InventoryModelForm from './InventoryModelForm';
import InventoryAutoForm from './InventoryAutoForm';
import InventoryAutosList from './InventoryAutoList';
import ServiceHistory from './ServiceRecords';


function App(props) {
  let {sales_employees, salesRecords, salesCustomers, soldVINS, inventoryAutos, serviceTechnicians, serviceAppointments,} = props

  
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales_employees">
            <Route 
              index 
              element={<SalesEmployeeList 
                sales_employees={sales_employees} 
              />} 
            />
            <Route 
              path="new" 
              element={<SalesEmployeeForm 
              />} 
            />
            <Route
              path="history"
              element={<SalesEmployeeHistory 
                salesRecords={salesRecords}
                salesEmployees={sales_employees}
              />} 
            />
          </Route>
          <Route path="records">
            <Route 
              index 
              element={<SalesRecordList 
                salesRecords={salesRecords}

              />} 
            />
            <Route 
              path="new" 
              element={<SalesRecordForm 
                soldVINS={soldVINS}
              />} 
            />
          </Route>
          <Route path="sales_customers">
            <Route 
              index
              element={<SaleCustomerList
                salesCustomers={salesCustomers}
              />}
            />
            <Route 
              path="new" 
              element={<SalesCustomerForm 
              />} 
            />
          </Route>
          <Route path="technicians">
            <Route 
              index 
              element={<TechniciansList 
                serviceTechnicians={props.serviceTechnicians}
              />} 
            />
            <Route path="new" 
              element={<TechniciansForm 
              />} 
            />
          </Route>
          <Route path="appointments">
            <Route 
              index 
              element={<AppointmentsList 
                serviceAppointments={props.serviceAppointments}
              />} 
            />
            <Route 
              path="new" 
              element={<AppointmentsForm 
                salesRecords={salesRecords}
              />} 
            />
            <Route
              path='service_history'
              element={<ServiceHistory 
                serviceAppointments={serviceAppointments}
              />}
            />
          </Route>
          <Route path="inventory">
            <Route 
              index 
              element={<InventoryAutosList 
                inventoryAutos={inventoryAutos}
              />} 
            />
            <Route
              path="new_auto"
              element={<InventoryAutoForm
              />} 
            />
            <Route
              path="new_make"
              element={<InventoryMakeForm
              />}
            />
            <Route
              path="new_model"
              element={<InventoryModelForm
              />} 
            />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
