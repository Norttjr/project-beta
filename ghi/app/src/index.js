import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadCarCarViews() {
  let salesEmployeeData, salesRecordData, salesCustomerData, serviceTechnicianData, serviceAppointmentData, soldVINSData, inventoryAutosData
  
  const inventoryAutosresponse = await fetch('http://localhost:8100/api/automobiles/')
  const salesRecordResponse = await fetch('http://localhost:8090/api/records/');
  const salesEmployeeResponse = await fetch('http://localhost:8090/api/sales_employees/')
  const salesCustomerResponse = await fetch('http://localhost:8090/api/sales_customers/')
  const serviceTechnicianResponse = await fetch('http://localhost:8080/api/technicians/')
  const serviceAppointmentResponse = await fetch('http://localhost:8080/api/appointments/')
  const soldVINSResponse = await fetch('http://localhost:8090/api/records/vins')

  if (inventoryAutosresponse.ok) {
    inventoryAutosData = await inventoryAutosresponse.json();
    console.log('cars in inventory', inventoryAutosData)
  }
  if(soldVINSResponse.ok) {
    soldVINSData = await soldVINSResponse.json();
    console.log('sold vins data:', soldVINSData)
  }
  if(salesRecordResponse.ok) {
    salesRecordData = await salesRecordResponse.json();
    console.log('sales record data:', salesRecordData)
  } else {
    console.error(salesRecordResponse);
  }
  if(salesEmployeeResponse.ok) {
    salesEmployeeData = await salesEmployeeResponse.json();
    console.log('sales employee data:', salesEmployeeData)
  } else {
    console.error(salesEmployeeResponse)
  }
  if(salesCustomerResponse.ok) {
    salesCustomerData = await salesCustomerResponse.json();
    console.log("sales customer data:", salesCustomerData)
  } else {
    console.error(salesCustomerResponse)
  }
  if(serviceAppointmentResponse.ok) {
    serviceAppointmentData = await serviceAppointmentResponse.json();
    console.log("service appointment data:", serviceAppointmentData)
  } else {
    console.error(serviceAppointmentResponse)
  }
  if(serviceTechnicianResponse.ok) {
    serviceTechnicianData = await serviceTechnicianResponse.json();
    console.log("service technician data:", serviceTechnicianData)
  } else {
    console.error(serviceTechnicianResponse)
  }
  
  root.render(
    <React.StrictMode>
      <App 
      inventoryAutos={inventoryAutosData.autos}
      soldVINS = {soldVINSData}
      sales_employees={salesEmployeeData} 
      salesRecords={salesRecordData} 
      salesCustomers={salesCustomerData} 
      serviceTechnicians={serviceTechnicianData} 
      serviceAppointments={serviceAppointmentData}
      />
    </React.StrictMode>
  );
}
loadCarCarViews();
