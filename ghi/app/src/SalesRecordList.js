function SalesRecordList({salesRecords}) {
    return (
        <div className="container">
            <h2 className="display-5 fw-bold">Records</h2>
            <a href='new/'>
                <h5>New Sale</h5>
            </a>
            <div className="row">
                {salesRecords.map(record => {
                    return (
                        <div key={record.id} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                    <h5 className="card-subtitle mb-2 text-muted">Date: {record.date}</h5>
                                    <h6 className="card-subtitle mb-2">Price: {record.price}</h6>
                                    <p className="card-text">
                                        Vin Number: {record.automobile.vin}
                                    </p>
                                    <p className="card-text">
                                        Customer: {record.customer.name}
                                    </p>
                                    <p className="card-text">
                                        Sales Person: {record.sales_person.name}
                                    </p>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>   
            <a href='https://www.youtube.com/watch?v=dQw4w9WgXcQ'>
                <h5>DONT CLICK ME</h5>
            </a>     
        </div>
    );
}
export default SalesRecordList;