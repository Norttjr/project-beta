import React from "react";

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: "",
            automobile: "",
            automobiles: [],
            sales_person: "",
            sales_employees: [],
            customer: "",
            sales_customers: [],
            
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        const url2 = 'http://localhost:8090/api/sales_employees/';
        const url3 = 'http://localhost:8090/api/sales_customers/';
        
        const response = await fetch(url);
        const response2 = await fetch(url2);
        const response3 = await fetch(url3);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({ automobiles: data.autos });
        }
        if (response2.ok) {
            const data2 = await response2.json();
            this.setState({ sales_employees: data2 });
        }
        if (response3.ok) {
          const data3 = await response3.json();
          this.setState({ sales_customers: data3 });
        }
      }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.automobiles;
        delete data.sales_customers;
        delete data.sales_employees;

        const locationUrl = 'http://localhost:8090/api/records/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            const newRecord = await response.json();
            console.log(newRecord);
            this.setState({
                price: "",
                automobile: "",
                sales_person: "",
                customer: "",
            })
            window.location.reload(false);
        }
    }
    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value});
    }
    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({automobile: value})
    }
    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({sales_person: value})
    }
    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({customer: value})
    }
   
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangePrice} value={this.state.price} placeholder="Price" required type = "number" name="price" id="price" className="form-control" />
                                <label htmlFor="price">Price</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeAutomobile}  value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {this.state.automobiles.filter(auto => {
                                    if (this.props.soldVINS.includes(auto.vin) === false) {
                                        return auto
                                    }
                                }).map(valid_auto => {
                                    return (
                                        <option 
                                            key={valid_auto.id} 
                                            value={valid_auto.href}
                                        >
                                            {valid_auto.vin}
                                        </option>
                                    );
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeCustomer} value={this.state.customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {this.state.sales_customers.map(customer => {
                                    return (
                                    <option key={customer.id} value={customer.name}>{customer.name}</option>
                                    )
                                })}
                                </select>
                                <a href='http://localhost:3000/sales_customers/new/'>
                                    New Customer
                                 </a>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                                <option value="">Choose a Sales Rep</option>
                                {this.state.sales_employees.map(employee => {
                                    return (
                                    <option key={employee.id} value={employee.name}>{employee.name}</option>
                                    )
                                })}
                                </select>
                                <a href='http://localhost:3000/sales_employees/new/'>
                                    New Sales Employee
                                </a>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default SalesRecordForm;