function SalesEmployeeList({sales_employees}) {
    
    return (
        <div className="container">
            <h2 className="display-5 fw-bold">Employees</h2>
            <a href='new/'>
                <h5>Add Employee</h5>
            </a>
            <a href='history/'>
                <h5>Employee Sales History</h5>
            </a>
            <div className="row">
                {sales_employees.map(employee => {
                    return (
                        <div key={employee.employee_number} className="col">
                            <div className="card mb-3 shadow">
                                <div className="card-body">
                                    <a href='/sales_employees/history/'>
                                        <h5 className="card-subtitle mb-2 text-muted">
                                        {employee.name}
                                        </h5>
                                    </a>
                                    <h6 className="card-subtitle mb-2 text-muted"> Employee Number: {employee.employee_number}</h6>
                                </div>
                            </div>
                        </div>
                    );
                })}    
            </div>        
        </div>
    );
}
export default SalesEmployeeList