from django.urls import path

from .views import (
    sales_records, 
    sales_customers, 
    sales_customer, 
    sales_employees, 
    sales_employee,
    api_sold_vins
)

urlpatterns = [
    path(
        "records/", 
        sales_records, 
        name="api_records"
    ),
    path(
        "sales_employees/", 
        sales_employees, 
        name="sales_employees"
    ),
    path(
        "sales_employees/<int:pk>/", 
        sales_employee, 
        name="show_sales_employee"
    ),
    path(
        "sales_customers/", 
        sales_customers, 
        name="sales_customers"
    ),
    path(
        "sales_customers/<int:pk>/", 
        sales_customer, 
        name="update_sales_customer"
    ),
    path(
        "records/vins/", 
        api_sold_vins, 
        name="api_sold_vins"
    ),
]