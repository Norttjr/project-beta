from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import (SalesEmployee, SalesRecord, Customer, AutomobileVO)
from common.json import ModelEncoder
import json

# Create your views here.
class SoldVinsEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vins",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]
    
class SalesEmployeeEncoder(ModelEncoder):
    model = SalesEmployee
    properties = [
        "id",
        "name",
        "employee_number",
    ]
    
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "price",
        "date",
        "automobile",
        "sales_person",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesEmployeeEncoder(),
        "customer": CustomerEncoder(),
    }
#filter(automobile.vin=vin)
def check_for_sale(car):
    records = SalesRecord.objects.all()
    for record in records:
        print(record["automobile"])
        
        if car == record:
            return True
    


@require_http_methods(["GET", "POST"])
def sales_records(request):
    if request.method == "GET":
        records = SalesRecord.objects.all()
        return JsonResponse(
            records,
            encoder=SalesRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
            print(content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile Does Not exist"},
                status=400,
            )
        try:
            sales_person = content["sales_person"]
            sales_employee = SalesEmployee.objects.get(name=sales_person)
            content["sales_person"] = sales_employee
        except SalesEmployee.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Employee Does Not exist"},
                status=400,
            )
        try:
            customer_name = content["customer"]
            sales_customer = Customer.objects.get(name=customer_name)
            content["customer"] = sales_customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message":"Customer Does Not Exist"},
                status=400
            )
        record = SalesRecord.objects.create(**content)
        return JsonResponse(
            record, 
            encoder=SalesRecordEncoder, 
            safe=False
            )

           
@require_http_methods(["GET", "POST"])
def sales_employees(request):
    if request.method == "POST":    
        content = json.loads(request.body)
        employee = SalesEmployee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder=SalesEmployeeEncoder,
            safe=False
        )
    else:
        employees = SalesEmployee.objects.all()
        return JsonResponse(
            employees,
            encoder=SalesEmployeeEncoder,
            safe=False
        )
        
@require_http_methods(["DELETE", "PUT", "GET"])
def sales_employee(request, pk):
    content = json.loads(request.body)
    employee = SalesEmployee.objects.get(id=pk)
    if request.method == "DELETE":
        try:
            employee.delete()   
            return JsonResponse(
                {"message": f"{employee} has been removed from the system"}
            )
        except SalesEmployee.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist."},
                status=400
            )     
    elif request.method == "PUT":
        employee.update(**content)
        number = employee["employee_number"]
        return JsonResponse(
            {"message": f"Employee {number}, has been updated"},
            employee,
            encoder=SalesEmployeeEncoder,
            safe=False
        )
    else: #GET
        number = employee["employee_number"]
        records = SalesRecord.objects.filter(sales_person=number)
        return JsonResponse(
            records,
            encoder=SalesRecordEncoder,
            safe=False
        )
        
@require_http_methods(["GET","POST"])
def sales_customers(request):
    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods(["GET","PUT"])
def sales_customer(request, pk):
    customer = Customer.objects.get(id=pk)
    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        customer.update(**content)
        name = customer["name"]
        return JsonResponse(
            {"message": f"Customer {name} has now been updated."},
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
        
@require_http_methods(["GET"])
def api_sold_vins(request):
    records = SalesRecord.objects.all()
    records_list = list(records)
    vins = []
    for record in records_list:
        vin = record
        vins.append(str(vin))
    return JsonResponse(
        vins,
        encoder=SoldVinsEncoder,
        safe=False
    )