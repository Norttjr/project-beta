from django.contrib import admin
from .models import Customer, SalesEmployee, SalesRecord
# Register your models here.



@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesEmployee)
class SalesEmployeeAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesRecord)
class SalesRecordAdmin(admin.ModelAdmin):
    pass