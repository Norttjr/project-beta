# CarCar
START UP INSTRUCTIONS:


docker volume create beta-data
docker compose build
docker compose up

go to "http://localhost:3000/" in your browser

click around!! make some data!!


Team:

* Carter - Sales microservice & Inventory frontend
* Norton - Service microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

The service department functions almost exclusively on its own as a separate entity. The only Models that are being used are the Technician model, and the Appointments model. There is a Costumer model that I would like to use to with the poller to poll for all the sales customers that have purchased from us, and recognize them by the vehicle vin number. This would allow us to automatically know if they should receive VIP service or not. Unfortunately I could not get that part done in time. 

Technicians can be added and recognized by their name and employee number (of their choosing). An appointment can be made on the Service Appointment page, where all appointment's will be listed This is done by inputting the vin number, costumer name, date & time, technician (from the selection techs in the system), and a reason for the appointment. Once an appointment is made, it will display on the page. An appointment can be marked either "canceled" or "completed" using the two buttons. Canceling will delete the appointment. Marking the appointment complete will allow it to be displayed on the Service History page. All Vin numbers will be listed on the Service History page but will not display any information about the appointment until it has been marked completed. 

In the future I would also like have all the details about the Appointments display on the Service records page, as well as recognize repeat customers by their Vin number.

note: after making an appointment you will have to refresh the page before it displays.


## Sales microservice


The sales department is self contained as seen in visualRoadmap.  I choose to keep the inventory as a root only going only to sales.  The only VO is the automobileVO we get with the sales poller. Me and norton decided that we could utilize the sales records as a reference for his vip eligibility and not have to use the service poller to make a SalesRecordVO in the service models.

The diagram shows the bounded context pretty well.  I did have to do a frankenView to get a list of sold vin#s (def api_sold_vins).  I then hit that end point in react to make it a prop that I could use to check against in the SalesRecord form.

In the browser I kept the employee features together and the sales record features together etc..  I really only wanted one link to get to a list and then I have links to "create" features that pertain to that domain of "records", "employees", etc...

Just to note:
    in order to make a sale you must first have at least 1 customer and at least 1 sales employee, these are foreign key relationships that populate in drop downs.  There are links in the sales form to make them.  But I figured I would let you know.

